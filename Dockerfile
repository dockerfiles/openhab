FROM java:8

ENV OPENHAB_VERSION 1.8.2

ADD https://bintray.com/artifact/download/openhab/bin/distribution-${OPENHAB_VERSION}-runtime.zip /tmp/openhab.zip
ADD https://bintray.com/artifact/download/openhab/bin/distribution-${OPENHAB_VERSION}-addons.zip /tmp/addons.zip

RUN unzip -q -d /opt/openhab /tmp/openhab.zip && \
	unzip -q -d /opt/openhab/addons-available /tmp/addons.zip && \
	rm /tmp/openhab.zip /tmp/addons.zip

CMD /opt/openhab/start.sh
